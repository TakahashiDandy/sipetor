/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sipetor.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author takahashidandy
 */
public class SQLConnection {
    private static Connection conn = null;
    
    public static Connection getConnection(){
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//            Class.forName("com.mysql.jdbc.Driver");
            conn = (DriverManager.getConnection(getSqlServer()));
            System.out.println("Connected to database !");
            
        }
        catch(SQLException sqle) {
           System.out.println("Sql Exception :"+sqle.getMessage());
        }
        catch(ClassNotFoundException e) {
         System.out.println("Class Not Found Exception :" + e.getMessage());
        }
      return conn;
    }
    
    private static String getSqlServer(){
        String conf = "jdbc:sqlserver://192.168.33.10:1433;"
                    + "databaseName=sipetor_23514098;"
                    + "user=sipetoradmin;"
                    + "password=Sipetor1234;" ;
        return conf;
    }
    
    private static String getMySqlServer(){
        String conf = "jdbc:mysql://localhost/sipetor?" 
                    +"user=root&password=" ;
        return conf;
    }
    
    
}
